#include <ring_queue.h>
#include <vector>
#include <deque>
#include <array>

#include <gtest/gtest.h>
#include <sstream>
#include <algorithm>

template <typename T> std::string content( const T &q )
{
	std::ostringstream ss;
	bool flag = false;

	for ( const auto &item : q )
	{
		if ( flag ) ss << ",";
		ss << item;
		flag = true;
	}

	return ss.str();
}

using namespace dull;

/* Constructor */

TEST( ring_queue, vector )
{
	ring_queue<int, std::vector<int>> q1;
	EXPECT_EQ( q1.max_size(), 0 );
	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );

	ring_queue<int, std::vector<int>> q2( 10, 10 );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.capacity(), 10 );
	EXPECT_EQ( q2.size(), 0 );
	EXPECT_EQ( content( q2 ), "" );
}

TEST( ring_queue, deque )
{
	ring_queue<int, std::deque<int>> q1;
	EXPECT_EQ( q1.max_size(), 0 );
	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );

	ring_queue<int, std::deque<int>> q2( 10, 10 );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.capacity(), 10 );
	EXPECT_EQ( q2.size(), 0 );
	EXPECT_EQ( content( q2 ), "" );
}

TEST( ring_queue, array )
{
	ring_queue<int, std::array<int,10>> q1;
	EXPECT_EQ( q1.max_size(), 10 );
	EXPECT_EQ( q1.capacity(), 10 );
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );

	ring_queue<int, std::array<int,20>> q2( 10 );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.capacity(), 20 );
	EXPECT_EQ( q2.size(), 0 );
	EXPECT_EQ( content( q2 ), "" );
}

/* Copy constructor & assignment */

TEST( ring_queue, copy_constructor )
{
	ring_queue<int> q1( 10 );
	q1.push( 1 );
	q1.push( 2 );

	ring_queue<int> q2( q1 );
	EXPECT_EQ( q1.max_size(), 10 );
	EXPECT_EQ( q1.size(), 2 );
	EXPECT_EQ( content( q1 ), "1,2" );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.size(), 2 );
	EXPECT_EQ( content( q2 ), "1,2" );
}

TEST( ring_queue, copy_assignment )
{
	ring_queue<int> q1( 10 );
	q1.push( 1 );
	q1.push( 2 );

	ring_queue<int> q2( 100 );
	q2 = q1;
	EXPECT_EQ( q1.max_size(), 10 );
	EXPECT_EQ( q1.size(), 2 );
	EXPECT_EQ( content( q1 ), "1,2" );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.size(), 2 );
	EXPECT_EQ( content( q2 ), "1,2" );
}

/* Move constructor & assignment */

TEST( ring_queue, move_constructor )
{
	ring_queue<int> q1( 10 );
	q1.push( 1 );
	q1.push( 2 );

	ring_queue<int> q2( std::move(q1) );
//	EXPECT_EQ( q1.max_size(), 0 );
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.size(), 2 );
	EXPECT_EQ( content( q2 ), "1,2" );
}

TEST( ring_queue, move_assignment )
{
	ring_queue<int> q1( 10 );
	q1.push( 1 );
	q1.push( 2 );

	ring_queue<int> q2( 100 );
	q2 = std::move(q1);
//	EXPECT_EQ( q1.max_size(), 0 );
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );
	EXPECT_EQ( q2.max_size(), 10 );
	EXPECT_EQ( q2.size(), 2 );
	EXPECT_EQ( content( q2 ), "1,2" );
}

/* Element access */

TEST( ring_queue, at )
{
	ring_queue<int> q( 3 );

	q.push( 1 );
	q.push( 2 );
	EXPECT_EQ( q.at( 0 ), 1 );
	EXPECT_EQ( q.at( 1 ), 2 );
	EXPECT_THROW( q.at( 2 ), std::out_of_range );
	EXPECT_THROW( q.at( 3 ), std::out_of_range );

	q.push( 3 );
	q.push( 4 );
	EXPECT_EQ( q.at( 0 ), 2 );
	EXPECT_EQ( q.at( 1 ), 3 );
	EXPECT_EQ( q.at( 2 ), 4 );

	EXPECT_THROW( q.at( 3 ), std::out_of_range );
}

TEST( ring_queue, subscript_operator )
{
	ring_queue<int> q( 3 );

	q.push( 1 );
	q.push( 2 );
	EXPECT_EQ( q[0], 1 );
	EXPECT_EQ( q[1], 2 );

	q.push( 3 );
	q.push( 4 );
	EXPECT_EQ( q[0], 2 );
	EXPECT_EQ( q[1], 3 );
	EXPECT_EQ( q[2], 4 );
}

TEST( ring_queue, front )
{
	ring_queue<int> q( 3 );

	q.push( 1 );
	EXPECT_EQ( q.front(), 1 );

	q.push( 2 );
	EXPECT_EQ( q.front(), 1 );

	q.push( 3 );
	EXPECT_EQ( q.front(), 1 );

	q.push( 4 );
	EXPECT_EQ( q.front(), 2 );

	q.pop();
	EXPECT_EQ( q.front(), 3 );

	q.pop();
	EXPECT_EQ( q.front(), 4 );
}

TEST( ring_queue, back )
{
	ring_queue<int> q( 3 );

	q.push( 1 );
	EXPECT_EQ( q.back(), 1 );

	q.push( 2 );
	EXPECT_EQ( q.back(), 2 );

	q.push( 3 );
	EXPECT_EQ( q.back(), 3 );

	q.push( 4 );
	EXPECT_EQ( q.back(), 4 );

	q.pop();
	EXPECT_EQ( q.back(), 4 );

	q.pop();
	EXPECT_EQ( q.back(), 4 );
}

/* Iterators */

TEST( ring_queue_iterator, pre_increment )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.begin();
	EXPECT_EQ( it1, q.begin() );
	EXPECT_NE( it1, q.end() );
	EXPECT_EQ( *it1, 1 );

	auto it2 = ++it1;
	EXPECT_NE( it1, q.begin() );
	EXPECT_NE( it1, q.end() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, q.begin() );
	EXPECT_NE( it2, q.end() );
	EXPECT_EQ( *it2, 2 );

	auto it3 = ++it1;
	EXPECT_NE( it1, q.begin() );
	EXPECT_EQ( it1, q.end() );
	EXPECT_EQ( it3, it1 );
	EXPECT_NE( it3, q.begin() );
	EXPECT_EQ( it3, q.end() );
}

TEST( ring_queue_iterator, post_increment )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.cbegin();
	EXPECT_EQ( it1, q.cbegin() );
	EXPECT_NE( it1, q.cend() );
	EXPECT_EQ( *it1, 1 );

	auto it2 = it1++;
	EXPECT_NE( it1, q.cbegin() );
	EXPECT_NE( it1, q.cend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_NE( it2, it1 );
	EXPECT_EQ( it2, q.cbegin() );
	EXPECT_NE( it2, q.cend() );
	EXPECT_EQ( *it2, 1 );

	auto it3 = it1++;
	EXPECT_NE( it1, q.cbegin() );
	EXPECT_EQ( it1, q.cend() );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, q.cbegin() );
	EXPECT_NE( it3, q.cend() );
	EXPECT_EQ( *it3, 2 );
}

TEST( ring_queue_iterator, pre_decrement )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.end();
	EXPECT_NE( it1, q.begin() );
	EXPECT_EQ( it1, q.end() );

	auto it2 = --it1;
	EXPECT_NE( it1, q.begin() );
	EXPECT_NE( it1, q.end() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, q.begin() );
	EXPECT_NE( it2, q.end() );
	EXPECT_EQ( *it2, 2 );

	auto it3 = --it1;
	EXPECT_EQ( it1, q.begin() );
	EXPECT_NE( it1, q.end() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_EQ( it3, it1 );
	EXPECT_EQ( it3, q.begin() );
	EXPECT_NE( it3, q.end() );
	EXPECT_EQ( *it3, 1 );
}

TEST( ring_queue_iterator, post_decrement )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.cend();
	EXPECT_NE( it1, q.cbegin() );
	EXPECT_EQ( it1, q.cend() );

	auto it2 = it1--;
	EXPECT_NE( it1, q.cbegin() );
	EXPECT_NE( it1, q.cend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_NE( it2, it1 );
	EXPECT_NE( it2, q.cbegin() );
	EXPECT_EQ( it2, q.cend() );

	auto it3 = it1--;
	EXPECT_EQ( it1, q.cbegin() );
	EXPECT_NE( it1, q.cend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, q.cbegin() );
	EXPECT_NE( it3, q.cend() );
	EXPECT_EQ( *it3, 2 );
}

TEST( ring_queue_iterator, comparison )
{
	ring_queue<int> q( 3 );

	EXPECT_EQ( q.begin(), q.end() );

	q.push( 1 );

	auto it = q.begin();
	EXPECT_EQ( it, q.begin() );
	EXPECT_NE( it, q.end() );
	EXPECT_EQ( ++it, q.end() );
	EXPECT_EQ( --it, q.begin() );
}

TEST( ring_queue_iterator, indirection )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it = q.begin();
	EXPECT_EQ( *it, 1 );
	EXPECT_EQ( *++it, 2 );
}

TEST( ring_queue_iterator, member_of_pointer )
{
	struct A { int a; } a;
	ring_queue<A> q( 3 );
	a.a = 1;
	q.push( a );
	a.a = 2;
	q.push( a );

	auto it = q.begin();
	EXPECT_EQ( it->a, 1 );
	EXPECT_EQ( (++it)->a, 2 );
}

TEST( ring_queue_iterator, subtraction )
{
	ring_queue<int> q( 3 );

	EXPECT_EQ( q.begin() - q.begin(), 0 );
	EXPECT_EQ( q.end() - q.end(), 0 );
	EXPECT_EQ( q.end() - q.begin(), 0 );
	EXPECT_EQ( q.begin() - q.end(), 0 );

	q.push( 1 );
	q.push( 2 );

	EXPECT_EQ( q.begin() - q.begin(), 0 );
	EXPECT_EQ( q.end() - q.end(), 0 );
	EXPECT_EQ( q.end() - q.begin(), 2 );
	EXPECT_EQ( q.begin() - q.end(), -2 );

	q.push( 3 );

	EXPECT_EQ( q.begin() - q.begin(), 0 );
	EXPECT_EQ( q.end() - q.end(), 0 );
	EXPECT_EQ( q.end() - q.begin(), 3 );
	EXPECT_EQ( q.begin() - q.end(), -3 );

	q.push( 4 );

	EXPECT_EQ( q.begin() - q.begin(), 0 );
	EXPECT_EQ( q.end() - q.end(), 0 );
	EXPECT_EQ( q.end() - q.begin(), 3 );
	EXPECT_EQ( q.begin() - q.end(), -3 );

	auto it1 = q.begin();
	auto it2 = q.begin();

	++it2;
	EXPECT_EQ( it2 - it1, 1 );
	EXPECT_EQ( it1 - it2, -1 );

	++it2;
	EXPECT_EQ( it2 - it1, 2 );
	EXPECT_EQ( it1 - it2, -2 );
}

TEST( ring_queue_reverse_iterator, pre_increment )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.rbegin();
	EXPECT_EQ( it1, q.rbegin() );
	EXPECT_NE( it1, q.rend() );
	EXPECT_EQ( *it1, 2 );

	auto it2 = ++it1;
	EXPECT_NE( it1, q.rbegin() );
	EXPECT_NE( it1, q.rend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, q.rbegin() );
	EXPECT_NE( it2, q.rend() );
	EXPECT_EQ( *it2, 1 );

	auto it3 = ++it1;
	EXPECT_NE( it1, q.rbegin() );
	EXPECT_EQ( it1, q.rend() );
	EXPECT_EQ( it3, it1 );
	EXPECT_NE( it3, q.rbegin() );
	EXPECT_EQ( it3, q.rend() );
}

TEST( ring_queue_reverse_iterator, post_increment )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.crbegin();
	EXPECT_EQ( it1, q.crbegin() );
	EXPECT_NE( it1, q.crend() );
	EXPECT_EQ( *it1, 2 );

	auto it2 = it1++;
	EXPECT_NE( it1, q.crbegin() );
	EXPECT_NE( it1, q.crend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_NE( it2, it1 );
	EXPECT_EQ( it2, q.crbegin() );
	EXPECT_NE( it2, q.crend() );
	EXPECT_EQ( *it2, 2 );

	auto it3 = it1++;
	EXPECT_NE( it1, q.crbegin() );
	EXPECT_EQ( it1, q.crend() );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, q.crbegin() );
	EXPECT_NE( it3, q.crend() );
	EXPECT_EQ( *it3, 1 );
}

#if 0  // TODO
TEST( ring_queue_reverse_iterator, pre_decrement )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.rend();
	EXPECT_NE( it1, q.rbegin() );
	EXPECT_EQ( it1, q.rend() );

	auto it2 = --it1;
	EXPECT_NE( it1, q.rbegin() );
	EXPECT_NE( it1, q.rend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, q.rbegin() );
	EXPECT_NE( it2, q.rend() );
	EXPECT_EQ( *it2, 1 );

	auto it3 = --it1;
	EXPECT_EQ( it1, q.rbegin() );
	EXPECT_NE( it1, q.rend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_EQ( it3, it1 );
	EXPECT_EQ( it3, q.rbegin() );
	EXPECT_NE( it3, q.rend() );
	EXPECT_EQ( *it3, 2 );
}

TEST( ring_queue_reverse_iterator, post_decrement )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it1 = q.crend();
	EXPECT_NE( it1, q.crbegin() );
	EXPECT_EQ( it1, q.crend() );

	auto it2 = it1--;
	EXPECT_NE( it1, q.crbegin() );
	EXPECT_NE( it1, q.crend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_NE( it2, it1 );
	EXPECT_NE( it2, q.crbegin() );
	EXPECT_EQ( it2, q.crend() );

	auto it3 = it1--;
	EXPECT_EQ( it1, q.crbegin() );
	EXPECT_NE( it1, q.crend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, q.crbegin() );
	EXPECT_NE( it3, q.crend() );
	EXPECT_EQ( *it3, 1 );
}
#endif

TEST( ring_queue_reverse_iterator, comparison )
{
	ring_queue<int> q( 3 );

	EXPECT_EQ( q.rbegin(), q.rend() );

	q.push( 1 );

	auto it = q.rbegin();
	EXPECT_EQ( it, q.rbegin() );
	EXPECT_NE( it, q.rend() );
	EXPECT_EQ( ++it, q.rend() );
//	EXPECT_EQ( --it, q.rbegin() );  // TODO
}

TEST( ring_queue_reverse_iterator, indirection )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );

	auto it = q.rbegin();
	EXPECT_EQ( *it, 2 );
	EXPECT_EQ( *++it, 1 );
}

TEST( ring_queue_reverse_iterator, member_of_pointer )
{
	struct A { int a; } a;
	ring_queue<A> q( 3 );
	a.a = 1;
	q.push( a );
	a.a = 2;
	q.push( a );

	auto it = q.rbegin();
	EXPECT_EQ( it->a, 2 );
	EXPECT_EQ( (++it)->a, 1 );
}

#if 0  // TODO
TEST( ring_queue_reverse_iterator, subtraction )
{
	ring_queue<int> q( 3 );

	EXPECT_EQ( q.rbegin() - q.rbegin(), 0 );
	EXPECT_EQ( q.rend() - q.rend(), 0 );
	EXPECT_EQ( q.rend() - q.rbegin(), 0 );
	EXPECT_EQ( q.rbegin() - q.rend(), 0 );

	q.push( 1 );
	q.push( 2 );

	EXPECT_EQ( q.rbegin() - q.rbegin(), 0 );
	EXPECT_EQ( q.rend() - q.rend(), 0 );
	EXPECT_EQ( q.rend() - q.rbegin(), 2 );
	EXPECT_EQ( q.rbegin() - q.rend(), -2 );

	q.push( 3 );

	EXPECT_EQ( q.rbegin() - q.rbegin(), 0 );
	EXPECT_EQ( q.rend() - q.rend(), 0 );
	EXPECT_EQ( q.rend() - q.rbegin(), 3 );
	EXPECT_EQ( q.rbegin() - q.rend(), -3 );

	q.push( 4 );

	EXPECT_EQ( q.rbegin() - q.rbegin(), 0 );
	EXPECT_EQ( q.rend() - q.rend(), 0 );
	EXPECT_EQ( q.rend() - q.rbegin(), 3 );
	EXPECT_EQ( q.rbegin() - q.rend(), -3 );

	auto it1 = q.rbegin();
	auto it2 = q.rbegin();

	++it2;
	EXPECT_EQ( it2 - it1, 1 );
	EXPECT_EQ( it1 - it2, -1 );

	++it2;
	EXPECT_EQ( it2 - it1, 2 );
	EXPECT_EQ( it1 - it2, -2 );
}
#endif

/* Capacity */

TEST( ring_queue_capacity, empty )
{
	ring_queue<int> q1;
	ring_queue<int> q2( 3 );

	EXPECT_TRUE( q1.empty() );
	EXPECT_TRUE( q2.empty() );

	q1.push( 1 );
	q2.push( 1 );

	EXPECT_TRUE( q1.empty() );
	EXPECT_FALSE( q2.empty() );

	q2.pop();

	EXPECT_TRUE( q2.empty() );
}

TEST( ring_queue_capacity, full )
{
	ring_queue<int> q1;
	ring_queue<int> q2( 3 );

	EXPECT_TRUE( q1.full() );
	EXPECT_FALSE( q2.full() );

	q1.push( 1 );
	q2.push( 1 );

	EXPECT_TRUE( q1.full() );
	EXPECT_FALSE( q2.full() );

	q1.push( 2 );
	q2.push( 2 );

	EXPECT_TRUE( q1.full() );
	EXPECT_FALSE( q2.full() );

	q1.push( 3 );
	q2.push( 3 );

	EXPECT_TRUE( q1.full() );
	EXPECT_TRUE( q2.full() );

	q2.pop();

	EXPECT_TRUE( q1.full() );
	EXPECT_FALSE( q2.full() );
}

TEST( ring_queue_capacity, size )
{
	ring_queue<int> q1;
	ring_queue<int> q2( 3, 3 );

	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( q2.size(), 0 );

	q1.push( 1 );
	q2.push( 1 );

	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( q2.size(), 1 );

	q1.push( 2 );
	q2.push( 2 );

	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( q2.size(), 2 );

	q1.push( 3 );
	q2.push( 3 );

	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( q2.size(), 3 );

	q2.pop();

	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( q2.size(), 2 );
}

TEST( ring_queue_capacity, max_size )
{
	ring_queue<int> q1;
	ring_queue<int> q2( 3, 2 );

	EXPECT_EQ( q1.max_size(), 0 );
	EXPECT_EQ( q2.max_size(), 3 );

	q2.push( 1 );
	q2.push( 2 );
	q2.push( 3 );

	EXPECT_TRUE( q2.full() );
	EXPECT_EQ( q2.max_size(), q2.size() );

	q2.push( 4 );

	EXPECT_TRUE( q2.full() );
	EXPECT_EQ( q2.max_size(), q2.size() );

	q1.set_max_size( 10 );
	q2.set_max_size( 10 );

	EXPECT_EQ( q1.max_size(), 10 );
	EXPECT_EQ( q2.max_size(), 10 );
}

TEST( ring_queue_capacity, capacity )
{
	ring_queue<int> q1;
	ring_queue<int> q2( 3 );
	ring_queue<int> q3( 3, 2 );
	ring_queue<int> q4( 3, 3 );

	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q2.capacity(), 0 );
	EXPECT_EQ( q3.capacity(), 2 );
	EXPECT_EQ( q4.capacity(), 3 );

	q1.push( 1 );
	q2.push( 1 );
	q3.push( 1 );
	q4.push( 1 );

	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q2.capacity(), 1 );
	EXPECT_EQ( q3.capacity(), 2 );
	EXPECT_EQ( q4.capacity(), 3 );

	q1.push( 2 );
	q2.push( 2 );
	q3.push( 2 );
	q4.push( 2 );

	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q2.capacity(), 2 );
	EXPECT_EQ( q3.capacity(), 2 );
	EXPECT_EQ( q4.capacity(), 3 );

	q1.push( 3 );
	q2.push( 3 );
	q3.push( 3 );
	q4.push( 3 );

	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q2.capacity(), 3 );
	EXPECT_EQ( q3.capacity(), 3 );
	EXPECT_EQ( q4.capacity(), 3 );

	q1.push( 4 );
	q2.push( 4 );
	q3.push( 4 );
	q4.push( 4 );

	EXPECT_EQ( q1.capacity(), 0 );
	EXPECT_EQ( q2.capacity(), 3 );
	EXPECT_EQ( q3.capacity(), 3 );
	EXPECT_EQ( q4.capacity(), 3 );

	q1.set_max_size( 10, 3 );
	q2.set_max_size( 10, 2 );
	q3.set_max_size( 10, 1 );
	q4.set_max_size( 10, 0 );

	EXPECT_EQ( q1.capacity(), 3 );
	EXPECT_EQ( q2.capacity(), 2 );
	EXPECT_EQ( q3.capacity(), 1 );
	EXPECT_EQ( q4.capacity(), 0 );
}

TEST( ring_queue_capacity, shrink_to_fit )
{
	// Since shrink_to_fit() needs to re-order the elements,
	// and since this should work fine regardless of the arragement
	// of the elements in the data container, we test multiple
	// sizes and arragements here.

	for ( size_t max_size = 0; max_size <= 10; max_size++ )
	{
		for ( size_t i = 0; i < 2 * max_size; i++ )
		{
			auto n = std::min( i, max_size );

			for ( size_t j = 0; j < n; j++ )
			{
				ring_queue<int> q( max_size );

				// push <i> elements
				for ( size_t k = 0; k < i; k++ ) q.push( k );

				// pop <j> elements
				for ( size_t k = 0; k < j; k++ ) q.pop();

				ASSERT_EQ( q.size(), n - j );

				auto s = content( q );
				q.shrink_to_fit();

				EXPECT_EQ( content( q ), s     ) << "max_size=" << max_size << "i=" << i << ", j=" << j;
				EXPECT_EQ( q.capacity(), n - j ) << "max_size=" << max_size << "i=" << i << ", j=" << j;
				EXPECT_EQ( q.size()    , n - j ) << "max_size=" << max_size << "i=" << i << ", j=" << j;
			}
		}
	}

	// shrink_to_fit() on an uninitialized queue should make no harm
	ring_queue<int> q;
	ASSERT_TRUE( q.empty() );
	q.shrink_to_fit();
	EXPECT_TRUE( q.empty() );
}

TEST( ring_queue_capacity, array )
{
	ring_queue<int, std::array<int,10>> q( 20 );
	EXPECT_EQ( q.max_size(), 20 );
	EXPECT_EQ( q.capacity(), 10 );
	EXPECT_EQ( q.size(), 0 );
	EXPECT_EQ( content( q ), "" );

	for ( int i = 0; i < 10; i++ ) q.push( i );
	EXPECT_EQ( q.size(), 10 );
	EXPECT_EQ( content( q ), "0,1,2,3,4,5,6,7,8,9" );

	EXPECT_THROW( q.push( 10 ), std::out_of_range );
}

/* Modifiers */

TEST( ring_queue, clear )
{
	ring_queue<int> q( 3 );

	q.clear();
	EXPECT_TRUE( q.empty() );
	EXPECT_EQ( q.capacity(), 0 );

	q.push( 1 );
	EXPECT_FALSE( q.empty() );
	EXPECT_EQ( q.capacity(), 1 );

	q.clear();
	EXPECT_TRUE( q.empty() );
	EXPECT_EQ( q.capacity(), 1 );

	q.push( 1 );
	q.push( 2 );
	q.push( 3 );
	q.push( 4 );
	EXPECT_FALSE( q.empty() );
	EXPECT_EQ( q.capacity(), 3 );

	q.clear();
	EXPECT_TRUE( q.empty() );
	EXPECT_EQ( q.capacity(), 3 );
}

TEST( ring_queue, push )
{
	ring_queue<std::string> q( 3 );

	std::string s = "Huba!";
	q.push( s );

	EXPECT_EQ( s, "Huba!" );
	EXPECT_EQ( q.front(), "Huba!" );
	EXPECT_EQ( q.back(), "Huba!" );

	s = "Hopp!";
	q.push( std::move( s ) );

	EXPECT_EQ( s, "" );
	EXPECT_EQ( q.front(), "Huba!" );
	EXPECT_EQ( q.back(), "Hopp!" );
}

TEST( ring_queue, pop )
{
	ring_queue<int> q( 3 );

	q.push( 1 );
	EXPECT_EQ( q.front(), 1 );
	EXPECT_EQ( q.back(), 1 );
	EXPECT_EQ( content( q ), "1" );

	q.pop();
	EXPECT_TRUE( q.empty() );
	EXPECT_EQ( content( q ), "" );

	q.push( 1 );
	EXPECT_EQ( q.front(), 1 );
	EXPECT_EQ( q.back(), 1 );
	EXPECT_EQ( content( q ), "1" );

	q.push( 2 );
	EXPECT_EQ( q.front(), 1 );
	EXPECT_EQ( q.back(), 2 );
	EXPECT_EQ( content( q ), "1,2" );

	q.pop();
	EXPECT_EQ( q.front(), 2 );
	EXPECT_EQ( q.back(), 2 );
	EXPECT_EQ( content( q ), "2" );

	q.push( 3 );
	EXPECT_EQ( q.front(), 2 );
	EXPECT_EQ( q.back(), 3 );
	EXPECT_EQ( content( q ), "2,3" );

	q.push( 4 );
	EXPECT_EQ( q.front(), 2 );
	EXPECT_EQ( q.back(), 4 );
	EXPECT_EQ( content( q ), "2,3,4" );

	q.push( 5 );
	EXPECT_EQ( q.front(), 3 );
	EXPECT_EQ( q.back(), 5 );
	EXPECT_EQ( content( q ), "3,4,5" );

	q.pop();
	EXPECT_EQ( q.front(), 4 );
	EXPECT_EQ( q.back(), 5 );
	EXPECT_EQ( content( q ), "4,5" );

	q.pop();
	EXPECT_EQ( q.front(), 5 );
	EXPECT_EQ( q.back(), 5 );
	EXPECT_EQ( content( q ), "5" );

	q.pop();
	EXPECT_TRUE( q.empty() );
	EXPECT_EQ( content( q ), "" );
}

TEST( ring_queue, swap )
{
	ring_queue<int> q1( 3 );
	q1.push( 1 );
	q1.push( 2 );
	q1.push( 3 );
	q1.push( 4 );

	ring_queue<int> q2( 10 );
	q2.push( 11 );
	q2.push( 12 );

	EXPECT_EQ( content( q1 ), "2,3,4" );
	EXPECT_EQ( content( q2 ), "11,12" );

	q1.swap( q2 );

	EXPECT_EQ( content( q2 ), "2,3,4" );
	EXPECT_EQ( content( q1 ), "11,12" );
}

/* Standard algorithms */

TEST( ring_queue, copy_algorithm )
{
	ring_queue<int> q( 3 );
	q.push( 1 );
	q.push( 2 );
	q.push( 3 );
	q.push( 4 );

	EXPECT_EQ( content( q ), "2,3,4" );

	std::vector<int> dest( 3 );
	std::copy( q.begin(), q.end(), dest.begin() );

	EXPECT_EQ( content( dest ), "2,3,4" );
}

TEST( ring_queue, move_algorithm )
{
	ring_queue<int> q( 4 );
	q.push( 1 );
	q.push( 2 );
	q.push( 3 );
	q.push( 4 );
	q.push( 5 );

	EXPECT_EQ( content( q ), "2,3,4,5" );

	auto first = q.begin(); ++first;
	auto last = q.end();
	auto d_first = q.begin();
	std::move( first, last, d_first );

	EXPECT_EQ( content( q ), "3,4,5,5" );
}

TEST( ring_queue, move_backward_algorithm )
{
	ring_queue<int> q( 4 );
	q.push( 1 );
	q.push( 2 );
	q.push( 3 );
	q.push( 4 );
	q.push( 5 );

	EXPECT_EQ( content( q ), "2,3,4,5" );

	auto first = q.begin();
	auto last = q.end(); --last;
	auto d_last = q.end();
	std::move_backward( first, last, d_last );

	EXPECT_EQ( content( q ), "2,2,3,4" );
}

