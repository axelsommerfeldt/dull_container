#include <simple_set.h>

#include <gtest/gtest.h>
#include <sstream>
#include <algorithm>

#include <vector>
#include <deque>
#include <list>
#include "poor_vector.h"

template <typename T> std::string content( const T &v )
{
	std::ostringstream ss;
	bool flag = false;

	for ( const auto &item : v )
	{
		if ( flag ) ss << ",";
		ss << item;
		flag = true;
	}

	return ss.str();
}

struct A
{
	std::string s;
	int i;

	A() : i( 0 ) {}
	A( const char *s, int i ) : s( s ), i( i ) {}

	A( const A &other ) = default;
	A( A &&other ) = default;
	A &operator=( const A &other ) = default;
	A &operator=( A &&other ) = default;

	bool operator==( const A &other ) const
	{
		return s == other.s && i == other.i;
	}

	friend std::ostream &operator<<( std::ostream &os, const A &a )
	{
		return os << a.s << ";" << a.i;
	}
};

using namespace dull;

/* Constructor */

TEST( simple_set, vector )
{
	simple_set<int, std::vector<int>> q1;
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );
}

TEST( simple_set, deque )
{
	simple_set<int, std::deque<int>> q1;
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );
}

TEST( simple_set, list )
{
	simple_set<int, std::list<int>> q1;
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );
}

TEST( simple_set, poor_vector )
{
	simple_set<int, poor_vector<int,10>> q1;
	EXPECT_EQ( q1.size(), 0 );
	EXPECT_EQ( content( q1 ), "" );
}

TEST( simple_set, copy_constructor )
{
	simple_set<int> v1;
	v1.insert( 1 );
	v1.insert( 2 );

	simple_set<int> v2( v1 );
	EXPECT_EQ( v1.size(), 2 );
	EXPECT_EQ( content( v1 ), "1,2" );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

TEST( simple_set, move_constructor )
{
	simple_set<int> v1;
	v1.insert( 1 );
	v1.insert( 2 );

	simple_set<int> v2( std::move(v1) );
	EXPECT_EQ( v1.size(), 0 );
	EXPECT_EQ( content( v1 ), "" );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

/* Assignment */

TEST( simple_set, copy_assignment )
{
	simple_set<int> v1;
	v1.insert( 1 );
	v1.insert( 2 );

	simple_set<int> v2;
	v2 = v1;
	EXPECT_EQ( v1.size(), 2 );
	EXPECT_EQ( content( v1 ), "1,2" );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

TEST( simple_set, move_assignment )
{
	simple_set<int> v1;
	v1.insert( 1 );
	v1.insert( 2 );

	simple_set<int> v2;
	v2 = std::move(v1);
	EXPECT_EQ( v1.size(), 0 );
	EXPECT_EQ( content( v1 ), "" );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

TEST( simple_set, assignment )
{
	simple_set<int> v = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	v = { 11, 12, 13, 14 };
	EXPECT_EQ( content( v ), "11,12,13,14" );
}

TEST( simple_set, assign )
{
	simple_set<int> v = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	std::vector<int> src = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	v.assign( src.begin() + 1, src.end() - 1 );
	EXPECT_EQ( content( v ), "1,2,3,4,5,6,7,8" );

	v.assign( { 1, 2, 3, 4 } );
	EXPECT_EQ( content( v ), "1,2,3,4" );
}

/* Element access */

TEST( simple_set, container )
{
	simple_set<int> v;

	v.container().push_back( 1 );

	EXPECT_EQ( v.size(), 1 );
	EXPECT_EQ( content( v ), "1" );
	EXPECT_TRUE( v.contains( 1 ) );

	v.container().push_back( 1 );

	EXPECT_EQ( v.size(), 2 );
	EXPECT_EQ( content( v ), "1,1" );
	EXPECT_TRUE( v.contains( 1 ) );
}

/* Iterators */

// Since simple_set just uses the iterators of the base class, it's of no use to test them

/* Capacity */

TEST( simple_set, empty )
{
	simple_set<int> v;

	EXPECT_TRUE( v.empty() );

	v.insert( 1 );

	EXPECT_FALSE( v.empty() );
}

TEST( simple_set, size )
{
	simple_set<int> v;

	EXPECT_EQ( v.size(), 0 );

	v.insert( 1 );

	EXPECT_EQ( v.size(), 1 );

	v.insert( 2 );

	EXPECT_EQ( v.size(), 2 );

	v.insert( 3 );

	EXPECT_EQ( v.size(), 3 );

	v.insert( 1 );

	EXPECT_EQ( v.size(), 3 );

	v.erase( 2 );

	EXPECT_EQ( v.size(), 2 );
}

/* Modifiers */

TEST( simple_set, clear )
{
	simple_set<int> v;

	v.clear();
	EXPECT_TRUE( v.empty() );
	EXPECT_EQ( v.size(), 0 );

	v.insert( 1 );
	EXPECT_FALSE( v.empty() );
	EXPECT_EQ( v.size(), 1 );

	v.clear();
	EXPECT_TRUE( v.empty() );
	EXPECT_EQ( v.size(), 0 );

	v.insert( 1 );
	v.insert( 2 );
	v.insert( 3 );
	v.insert( 4 );
	EXPECT_FALSE( v.empty() );
	EXPECT_EQ( v.size(), 4 );

	v.clear();
	EXPECT_TRUE( v.empty() );
	EXPECT_EQ( v.size(), 0 );
}

TEST( simple_set, insert )
{
	simple_set<int> v { 1, 2, 3, 5, 6, 7, 8, 9, 10 };

	v.insert( 4 );

	EXPECT_EQ( v.size(), 10 );
	EXPECT_EQ( content( v ), "1,2,3,5,6,7,8,9,10,4" );

	v.insert( 5 );

	EXPECT_EQ( v.size(), 10 );
	EXPECT_EQ( content( v ), "1,2,3,5,6,7,8,9,10,4" );

	auto it = v.insert( v.begin() + 1, 11 );

	EXPECT_EQ( v.size(), 11 );
	EXPECT_EQ( content( v ), "1,11,2,3,5,6,7,8,9,10,4" );
	EXPECT_EQ( *it, 11 );

	it = v.insert( v.begin() + 1, 7 );

	EXPECT_EQ( v.size(), 11 );
	EXPECT_EQ( content( v ), "1,11,2,3,5,6,7,8,9,10,4" );
	EXPECT_EQ( *it, 7 );

	simple_set<A> v2;

	A a1( "Huba!", 1 );
	v2.insert( a1 );

	EXPECT_EQ( v2.size(), 1 );
	EXPECT_EQ( a1.s, "Huba!" );

	A a2( "Hopp!", 2 );
	v2.insert( std::move( a2 ) );

	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( a2.s, "" );
}

#if 0  // TODO
TEST( simple_set, emplace )
{
	simple_set<A> v( 2 );
	v[0] = A( "Huba!", 1 );
	v[1] = A( "Hopp!", 2 );

	auto it1 = v.emplace( v.begin() + 1, "Test", 3 );

	EXPECT_EQ( v.size(), 3 );
	EXPECT_EQ( it1->s, "Test" );
	EXPECT_EQ( it1->i, 3 );
	EXPECT_EQ( content( v ), "Huba!;1,Test;3,Hopp!;2" );
}
#endif

TEST( simple_set, erase )
{
	simple_set<int> v { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	auto it1 = v.erase( v.begin() + 3 );  // erase 4

	EXPECT_EQ( v.size(), 9 );
	EXPECT_EQ( content( v ), "1,2,3,5,6,7,8,9,10" );
	EXPECT_EQ( *it1, 5 );

	auto it2 = v.erase( v.begin() + 4, v.end() - 1 );  // erase 6..9

	EXPECT_EQ( v.size(), 5 );
	EXPECT_EQ( content( v ), "1,2,3,5,10" );
	EXPECT_EQ( *it2, 10 );

	auto n1 = v.erase( 5 );

	EXPECT_EQ( v.size(), 4 );
	EXPECT_EQ( content( v ), "1,2,3,10" );
	EXPECT_EQ( n1, 1 );

	auto n2 = v.erase( 5 );

	EXPECT_EQ( v.size(), 4 );
	EXPECT_EQ( content( v ), "1,2,3,10" );
	EXPECT_EQ( n2, 0 );

	auto it3 = v.erase( v.begin(), v.end() );

	EXPECT_EQ( v.size(), 0 );
	EXPECT_EQ( content( v ), "" );
	EXPECT_EQ( it3, v.end() );
}

TEST( simple_set, swap )
{
	simple_set<int> v1;
	v1.insert( 1 );
	v1.insert( 2 );
	v1.insert( 3 );
	v1.insert( 4 );

	simple_set<int> v2;
	v2.insert( 11 );
	v2.insert( 12 );

	EXPECT_EQ( content( v1 ), "1,2,3,4" );
	EXPECT_EQ( content( v2 ), "11,12" );

	v1.swap( v2 );

	EXPECT_EQ( content( v2 ), "1,2,3,4" );
	EXPECT_EQ( content( v1 ), "11,12" );
}

/* Lookup */

TEST( simple_set, count )
{
	simple_set<int> v = { 1, 2, 3, 4 };

	EXPECT_EQ( v.count( 0 ), 0 );
	EXPECT_EQ( v.count( 1 ), 1 );
	EXPECT_EQ( v.count( 2 ), 1 );
	EXPECT_EQ( v.count( 3 ), 1 );
	EXPECT_EQ( v.count( 4 ), 1 );
	EXPECT_EQ( v.count( 5 ), 0 );

	v.insert( 3 );

	EXPECT_EQ( v.count( 3 ), 1 );

	v.insert( 5 );

	EXPECT_EQ( v.count( 5 ), 1 );

	v.erase( 2 );

	EXPECT_EQ( v.count( 2 ), 0 );
}

TEST( simple_set, find )
{
	simple_set<int> v = { 1, 2, 3, 4 };

	auto it = v.find( 0 );

	EXPECT_EQ( it, v.end() );

	it = v.find( 2 );

	ASSERT_NE( it, v.end() );
	EXPECT_EQ( *it, 2 );
}

TEST( simple_set, contains )
{
	simple_set<int> v = { 1, 2, 3, 4 };

	EXPECT_FALSE( v.contains( 0 ) );
	EXPECT_TRUE ( v.contains( 1 ) );
	EXPECT_TRUE ( v.contains( 2 ) );
	EXPECT_TRUE ( v.contains( 3 ) );
	EXPECT_TRUE ( v.contains( 4 ) );
	EXPECT_FALSE( v.contains( 5 ) );
}

/* Non-member functions */

TEST( simple_set, equal )
{
	simple_set<int> v1 { 1, 2, 3 };
	simple_set<int> v2 { 2, 3, 1 };
	simple_set<int> v3 { 1, 2, 4 };
	simple_set<int> v4 { 1, 2, 3, 4 };

	EXPECT_TRUE( v1 == v2 );
	EXPECT_FALSE( v1 == v3 );
	EXPECT_FALSE( v1 == v4 );

	EXPECT_TRUE( v2 == v1 );
	EXPECT_FALSE( v2 == v3 );
	EXPECT_FALSE( v2 == v4 );

	EXPECT_FALSE( v3 == v1 );
	EXPECT_FALSE( v3 == v2 );
	EXPECT_FALSE( v3 == v4 );

	EXPECT_FALSE( v4 == v1 );
	EXPECT_FALSE( v4 == v2 );
	EXPECT_FALSE( v4 == v3 );
}

/* Standard algorithms */

TEST( simple_set, copy_algorithm )
{
	simple_set<int> v = { 1, 2, 3, 4 };

	EXPECT_EQ( content( v ), "1,2,3,4" );

	std::vector<int> dest( 3 );
	std::copy( v.begin(), v.end() - 1, dest.begin() );

	EXPECT_EQ( content( dest ), "1,2,3" );
}

TEST( simple_set, move_algorithm )
{
	simple_set<int> v = { 1, 2, 3, 4 };

	EXPECT_EQ( content( v ), "1,2,3,4" );

	auto first = v.begin(); ++first;
	auto last = v.end();
	auto d_first = v.begin();
	std::move( first, last, d_first );

	EXPECT_EQ( content( v ), "2,3,4,4" );
}

TEST( simple_set, move_backward_algorithm )
{
	simple_set<int> v = { 1, 2, 3, 4 };

	EXPECT_EQ( content( v ), "1,2,3,4" );

	auto first = v.begin();
	auto last = v.end(); --last;
	auto d_last = v.end();
	std::move_backward( first, last, d_last );

	EXPECT_EQ( content( v ), "1,1,2,3" );
}

TEST( simple_set, sort_algorithm )
{
	simple_set<int> v = { 4, 3, 1, 2 };

	EXPECT_EQ( content( v ), "4,3,1,2" );

	std::sort( v.begin(), v.end() );

	EXPECT_EQ( content( v ), "1,2,3,4" );
}

