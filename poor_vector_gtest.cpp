#include <poor_vector.h>

#include <gtest/gtest.h>
#include <sstream>
#include <algorithm>
#include <vector>

template <typename T> std::string content( const T &v )
{
	std::ostringstream ss;
	bool flag = false;

	for ( const auto &item : v )
	{
		if ( flag ) ss << ",";
		ss << item;
		flag = true;
	}

	return ss.str();
}

struct A
{
	std::string s;
	int i;

	A() : i( 0 ) {}
	A( const char *s, int i ) : s( s ), i( i ) {}

	A( const A &other ) = default;
	A( A &&other ) = default;
	A &operator=( const A &other ) = default;
	A &operator=( A &&other ) = default;

	friend std::ostream &operator<<( std::ostream &os, const A &a )
	{
		return os << a.s << ";" << a.i;
	}
};

using namespace dull;

/* Constructor */

TEST( poor_vector, constructor )
{
	poor_vector<int, 10> v1;
	EXPECT_EQ( v1.max_size(), 10 );
	EXPECT_EQ( v1.capacity(), 10 );
	EXPECT_EQ( v1.size(), 0 );
	EXPECT_EQ( content( v1 ), "" );

	poor_vector<int, 10> v2( 10 );
	EXPECT_EQ( v2.max_size(), 10 );
	EXPECT_EQ( v2.capacity(), 10 );
	EXPECT_EQ( v2.size(), 10 );
//	EXPECT_EQ( content( v2 ), "" );

	poor_vector<int, 10> v3( 10, 1 );
	EXPECT_EQ( v3.max_size(), 10 );
	EXPECT_EQ( v3.capacity(), 10 );
	EXPECT_EQ( v3.size(), 10 );
	EXPECT_EQ( content( v3 ), "1,1,1,1,1,1,1,1,1,1" );

	std::vector<int> src = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	poor_vector<int,10> v4( src.begin() + 1, src.end() - 1 );
	EXPECT_EQ( v4.max_size(), 10 );
	EXPECT_EQ( v4.capacity(), 10 );
	EXPECT_EQ( v4.size(), 8 );
	EXPECT_EQ( content( v4 ), "1,2,3,4,5,6,7,8" );

	poor_vector<int, 10> v5 { 1, 2, 3, 4 };
	EXPECT_EQ( v5.max_size(), 10 );
	EXPECT_EQ( v5.capacity(), 10 );
	EXPECT_EQ( v5.size(), 4 );
	EXPECT_EQ( content( v5 ), "1,2,3,4" );
}

TEST( poor_vector, copy_constructor )
{
	poor_vector<int,10> v1;
	v1.push_back( 1 );
	v1.push_back( 2 );

	poor_vector<int,10> v2( v1 );
	EXPECT_EQ( v1.max_size(), 10 );
	EXPECT_EQ( v1.size(), 2 );
	EXPECT_EQ( content( v1 ), "1,2" );
	EXPECT_EQ( v2.max_size(), 10 );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

TEST( poor_vector, move_constructor )
{
	poor_vector<int,10> v1;
	v1.push_back( 1 );
	v1.push_back( 2 );

	poor_vector<int,10> v2( std::move(v1) );
	EXPECT_EQ( v1.max_size(), 10 );
	EXPECT_EQ( v1.size(), 0 );
	EXPECT_EQ( content( v1 ), "" );
	EXPECT_EQ( v2.max_size(), 10 );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

/* Assignment */

TEST( poor_vector, copy_assignment )
{
	poor_vector<int,10> v1;
	v1.push_back( 1 );
	v1.push_back( 2 );

	poor_vector<int,10> v2;
	v2 = v1;
	EXPECT_EQ( v1.max_size(), 10 );
	EXPECT_EQ( v1.size(), 2 );
	EXPECT_EQ( content( v1 ), "1,2" );
	EXPECT_EQ( v2.max_size(), 10 );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

TEST( poor_vector, move_assignment )
{
	poor_vector<int,10> v1;
	v1.push_back( 1 );
	v1.push_back( 2 );

	poor_vector<int,10> v2;
	v2 = std::move(v1);
	EXPECT_EQ( v1.max_size(), 10 );
	EXPECT_EQ( v1.size(), 0 );
	EXPECT_EQ( content( v1 ), "" );
	EXPECT_EQ( v2.max_size(), 10 );
	EXPECT_EQ( v2.size(), 2 );
	EXPECT_EQ( content( v2 ), "1,2" );
}

TEST( poor_vector, assignment )
{
	poor_vector<int, 10> v = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	v = { 11, 12, 13, 14 };
	EXPECT_EQ( content( v ), "11,12,13,14" );
}

TEST( poor_vector, assign )
{
	poor_vector<int, 10> v = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	v.assign( 3, 30 );
	EXPECT_EQ( content( v ), "30,30,30" );

	std::vector<int> src = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	v.assign( src.begin() + 1, src.end() - 1 );
	EXPECT_EQ( content( v ), "1,2,3,4,5,6,7,8" );

	v.assign( { 1, 2, 3, 4 } );
	EXPECT_EQ( content( v ), "1,2,3,4" );
}

/* Element access */

TEST( poor_vector, at )
{
	poor_vector<int,10> v;

	v.push_back( 1 );
	v.push_back( 2 );
	EXPECT_EQ( v.at( 0 ), 1 );
	EXPECT_EQ( v.at( 1 ), 2 );
	EXPECT_THROW( v.at( 2 ), std::out_of_range );
	EXPECT_THROW( v.at( 3 ), std::out_of_range );

	v.push_back( 3 );
	v.push_back( 4 );
	EXPECT_EQ( v.at( 0 ), 1 );
	EXPECT_EQ( v.at( 1 ), 2 );
	EXPECT_EQ( v.at( 2 ), 3 );
	EXPECT_EQ( v.at( 3 ), 4 );

	EXPECT_THROW( v.at( 10 ), std::out_of_range );
}

TEST( poor_vector, subscript_operator )
{
	poor_vector<int,10> v;

	v.push_back( 1 );
	v.push_back( 2 );
	EXPECT_EQ( v[0], 1 );
	EXPECT_EQ( v[1], 2 );

	v.push_back( 3 );
	v.push_back( 4 );
	EXPECT_EQ( v[0], 1 );
	EXPECT_EQ( v[1], 2 );
	EXPECT_EQ( v[2], 3 );
	EXPECT_EQ( v[3], 4 );
}

TEST( poor_vector, front )
{
	poor_vector<int,10> v;

	v.push_back( 1 );
	EXPECT_EQ( v.front(), 1 );

	v.push_back( 2 );
	EXPECT_EQ( v.front(), 1 );

	v.push_back( 3 );
	EXPECT_EQ( v.front(), 1 );

	v.push_back( 4 );
	EXPECT_EQ( v.front(), 1 );
}

TEST( poor_vector, back )
{
	poor_vector<int,10> v;

	v.push_back( 1 );
	EXPECT_EQ( v.back(), 1 );

	v.push_back( 2 );
	EXPECT_EQ( v.back(), 2 );

	v.push_back( 3 );
	EXPECT_EQ( v.back(), 3 );

	v.push_back( 4 );
	EXPECT_EQ( v.back(), 4 );
}

TEST( poor_vector, data )
{
	poor_vector<int,10> v = { 1, 2, 3, 4 };

	EXPECT_EQ( v.data()[0], 1 );
	EXPECT_EQ( v.data()[1], 2 );
	EXPECT_EQ( v.data()[2], 3 );
	EXPECT_EQ( v.data()[3], 4 );
}

/* Iterators */

TEST( poor_vector_iterator, pre_increment )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.begin();
	EXPECT_EQ( it1, v.begin() );
	EXPECT_NE( it1, v.end() );
	EXPECT_EQ( *it1, 1 );

	auto it2 = ++it1;
	EXPECT_NE( it1, v.begin() );
	EXPECT_NE( it1, v.end() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, v.begin() );
	EXPECT_NE( it2, v.end() );
	EXPECT_EQ( *it2, 2 );

	auto it3 = ++it1;
	EXPECT_NE( it1, v.begin() );
	EXPECT_EQ( it1, v.end() );
	EXPECT_EQ( it3, it1 );
	EXPECT_NE( it3, v.begin() );
	EXPECT_EQ( it3, v.end() );
}

TEST( poor_vector_iterator, post_increment )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.cbegin();
	EXPECT_EQ( it1, v.cbegin() );
	EXPECT_NE( it1, v.cend() );
	EXPECT_EQ( *it1, 1 );

	auto it2 = it1++;
	EXPECT_NE( it1, v.cbegin() );
	EXPECT_NE( it1, v.cend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_NE( it2, it1 );
	EXPECT_EQ( it2, v.cbegin() );
	EXPECT_NE( it2, v.cend() );
	EXPECT_EQ( *it2, 1 );

	auto it3 = it1++;
	EXPECT_NE( it1, v.cbegin() );
	EXPECT_EQ( it1, v.cend() );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, v.cbegin() );
	EXPECT_NE( it3, v.cend() );
	EXPECT_EQ( *it3, 2 );
}

TEST( poor_vector_iterator, pre_decrement )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.end();
	EXPECT_NE( it1, v.begin() );
	EXPECT_EQ( it1, v.end() );

	auto it2 = --it1;
	EXPECT_NE( it1, v.begin() );
	EXPECT_NE( it1, v.end() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, v.begin() );
	EXPECT_NE( it2, v.end() );
	EXPECT_EQ( *it2, 2 );

	auto it3 = --it1;
	EXPECT_EQ( it1, v.begin() );
	EXPECT_NE( it1, v.end() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_EQ( it3, it1 );
	EXPECT_EQ( it3, v.begin() );
	EXPECT_NE( it3, v.end() );
	EXPECT_EQ( *it3, 1 );
}

TEST( poor_vector_iterator, post_decrement )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.cend();
	EXPECT_NE( it1, v.cbegin() );
	EXPECT_EQ( it1, v.cend() );

	auto it2 = it1--;
	EXPECT_NE( it1, v.cbegin() );
	EXPECT_NE( it1, v.cend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_NE( it2, it1 );
	EXPECT_NE( it2, v.cbegin() );
	EXPECT_EQ( it2, v.cend() );

	auto it3 = it1--;
	EXPECT_EQ( it1, v.cbegin() );
	EXPECT_NE( it1, v.cend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, v.cbegin() );
	EXPECT_NE( it3, v.cend() );
	EXPECT_EQ( *it3, 2 );
}

TEST( poor_vector_iterator, comparison )
{
	poor_vector<int,10> v;

	EXPECT_EQ( v.begin(), v.end() );

	v.push_back( 1 );

	auto it = v.begin();
	EXPECT_EQ( it, v.begin() );
	EXPECT_NE( it, v.end() );
	EXPECT_EQ( ++it, v.end() );
	EXPECT_EQ( --it, v.begin() );
}

TEST( poor_vector_iterator, indirection )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it = v.begin();
	EXPECT_EQ( *it, 1 );
	EXPECT_EQ( *++it, 2 );
}

TEST( poor_vector_iterator, member_of_pointer )
{
	struct A { int a; } a;
	poor_vector<A,3> v;
	a.a = 1;
	v.push_back( a );
	a.a = 2;
	v.push_back( a );

	auto it = v.begin();
	EXPECT_EQ( it->a, 1 );
	EXPECT_EQ( (++it)->a, 2 );
}

TEST( poor_vector_iterator, subtraction )
{
	poor_vector<int,10> v;

	EXPECT_EQ( v.begin() - v.begin(), 0 );
	EXPECT_EQ( v.end() - v.end(), 0 );
	EXPECT_EQ( v.end() - v.begin(), 0 );
	EXPECT_EQ( v.begin() - v.end(), 0 );

	v.push_back( 1 );
	v.push_back( 2 );

	EXPECT_EQ( v.begin() - v.begin(), 0 );
	EXPECT_EQ( v.end() - v.end(), 0 );
	EXPECT_EQ( v.end() - v.begin(), 2 );
	EXPECT_EQ( v.begin() - v.end(), -2 );

	v.push_back( 3 );

	EXPECT_EQ( v.begin() - v.begin(), 0 );
	EXPECT_EQ( v.end() - v.end(), 0 );
	EXPECT_EQ( v.end() - v.begin(), 3 );
	EXPECT_EQ( v.begin() - v.end(), -3 );

	auto it1 = v.begin();
	auto it2 = v.begin();

	++it2;
	EXPECT_EQ( it2 - it1, 1 );
	EXPECT_EQ( it1 - it2, -1 );

	++it2;
	EXPECT_EQ( it2 - it1, 2 );
	EXPECT_EQ( it1 - it2, -2 );
}

#if 0
TEST( poor_vector_reverse_iterator, pre_increment )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.rbegin();
	EXPECT_EQ( it1, v.rbegin() );
	EXPECT_NE( it1, v.rend() );
	EXPECT_EQ( *it1, 2 );

	auto it2 = ++it1;
	EXPECT_NE( it1, v.rbegin() );
	EXPECT_NE( it1, v.rend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, v.rbegin() );
	EXPECT_NE( it2, v.rend() );
	EXPECT_EQ( *it2, 1 );

	auto it3 = ++it1;
	EXPECT_NE( it1, v.rbegin() );
	EXPECT_EQ( it1, v.rend() );
	EXPECT_EQ( it3, it1 );
	EXPECT_NE( it3, v.rbegin() );
	EXPECT_EQ( it3, v.rend() );
}

TEST( poor_vector_reverse_iterator, post_increment )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.crbegin();
	EXPECT_EQ( it1, v.crbegin() );
	EXPECT_NE( it1, v.crend() );
	EXPECT_EQ( *it1, 2 );

	auto it2 = it1++;
	EXPECT_NE( it1, v.crbegin() );
	EXPECT_NE( it1, v.crend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_NE( it2, it1 );
	EXPECT_EQ( it2, v.crbegin() );
	EXPECT_NE( it2, v.crend() );
	EXPECT_EQ( *it2, 2 );

	auto it3 = it1++;
	EXPECT_NE( it1, v.crbegin() );
	EXPECT_EQ( it1, v.crend() );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, v.crbegin() );
	EXPECT_NE( it3, v.crend() );
	EXPECT_EQ( *it3, 1 );
}

TEST( poor_vector_reverse_iterator, pre_decrement )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.rend();
	EXPECT_NE( it1, v.rbegin() );
	EXPECT_EQ( it1, v.rend() );

	auto it2 = --it1;
	EXPECT_NE( it1, v.rbegin() );
	EXPECT_NE( it1, v.rend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_EQ( it2, it1 );
	EXPECT_NE( it2, v.rbegin() );
	EXPECT_NE( it2, v.rend() );
	EXPECT_EQ( *it2, 1 );

	auto it3 = --it1;
	EXPECT_EQ( it1, v.rbegin() );
	EXPECT_NE( it1, v.rend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_EQ( it3, it1 );
	EXPECT_EQ( it3, v.rbegin() );
	EXPECT_NE( it3, v.rend() );
	EXPECT_EQ( *it3, 2 );
}

TEST( poor_vector_reverse_iterator, post_decrement )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it1 = v.crend();
	EXPECT_NE( it1, v.crbegin() );
	EXPECT_EQ( it1, v.crend() );

	auto it2 = it1--;
	EXPECT_NE( it1, v.crbegin() );
	EXPECT_NE( it1, v.crend() );
	EXPECT_EQ( *it1, 1 );
	EXPECT_NE( it2, it1 );
	EXPECT_NE( it2, v.crbegin() );
	EXPECT_EQ( it2, v.crend() );

	auto it3 = it1--;
	EXPECT_EQ( it1, v.crbegin() );
	EXPECT_NE( it1, v.crend() );
	EXPECT_EQ( *it1, 2 );
	EXPECT_NE( it3, it1 );
	EXPECT_NE( it3, v.crbegin() );
	EXPECT_NE( it3, v.crend() );
	EXPECT_EQ( *it3, 1 );
}

TEST( poor_vector_reverse_iterator, comparison )
{
	poor_vector<int,10> v;

	EXPECT_EQ( v.rbegin(), v.rend() );

	v.push_back( 1 );

	auto it = v.rbegin();
	EXPECT_EQ( it, v.rbegin() );
	EXPECT_NE( it, v.rend() );
	EXPECT_EQ( ++it, v.rend() );
	EXPECT_EQ( --it, v.rbegin() );
}

TEST( poor_vector_reverse_iterator, indirection )
{
	poor_vector<int,10> v;
	v.push_back( 1 );
	v.push_back( 2 );

	auto it = v.rbegin();
	EXPECT_EQ( *it, 2 );
	EXPECT_EQ( *++it, 1 );
}

TEST( poor_vector_reverse_iterator, member_of_pointer )
{
	struct A { int a; } a;
	ring_vueue<A> v( 3 );
	a.a = 1;
	v.push_back( a );
	a.a = 2;
	v.push_back( a );

	auto it = v.rbegin();
	EXPECT_EQ( it->a, 2 );
	EXPECT_EQ( (++it)->a, 1 );
}

TEST( poor_vector_reverse_iterator, subtraction )
{
	poor_vector<int,10> v;

	EXPECT_EQ( v.rbegin() - v.rbegin(), 0 );
	EXPECT_EQ( v.rend() - v.rend(), 0 );
	EXPECT_EQ( v.rend() - v.rbegin(), 0 );
	EXPECT_EQ( v.rbegin() - v.rend(), 0 );

	v.push_back( 1 );
	v.push_back( 2 );

	EXPECT_EQ( v.rbegin() - v.rbegin(), 0 );
	EXPECT_EQ( v.rend() - v.rend(), 0 );
	EXPECT_EQ( v.rend() - v.rbegin(), 2 );
	EXPECT_EQ( v.rbegin() - v.rend(), -2 );

	v.push_back( 3 );

	EXPECT_EQ( v.rbegin() - v.rbegin(), 0 );
	EXPECT_EQ( v.rend() - v.rend(), 0 );
	EXPECT_EQ( v.rend() - v.rbegin(), 3 );
	EXPECT_EQ( v.rbegin() - v.rend(), -3 );

	v.push_back( 4 );

	EXPECT_EQ( v.rbegin() - v.rbegin(), 0 );
	EXPECT_EQ( v.rend() - v.rend(), 0 );
	EXPECT_EQ( v.rend() - v.rbegin(), 3 );
	EXPECT_EQ( v.rbegin() - v.rend(), -3 );

	auto it1 = v.rbegin();
	auto it2 = v.rbegin();

	++it2;
	EXPECT_EQ( it2 - it1, 1 );
	EXPECT_EQ( it1 - it2, -1 );

	++it2;
	EXPECT_EQ( it2 - it1, 2 );
	EXPECT_EQ( it1 - it2, -2 );
}
#endif

/* Capacity */

TEST( poor_vector, empty )
{
	poor_vector<int,10> v;

	EXPECT_TRUE( v.empty() );

	v.push_back( 1 );

	EXPECT_FALSE( v.empty() );
}

TEST( poor_vector, size )
{
	poor_vector<int,10> v;

	EXPECT_EQ( v.size(), 0 );

	v.push_back( 1 );

	EXPECT_EQ( v.size(), 1 );

	v.push_back( 2 );

	EXPECT_EQ( v.size(), 2 );

	v.push_back( 3 );

	EXPECT_EQ( v.size(), 3 );
}

TEST( poor_vector, max_size )
{
	poor_vector<int,3> v;

	EXPECT_EQ( v.max_size(), 3 );

	v.push_back( 1 );
	v.push_back( 2 );
	v.push_back( 3 );

	EXPECT_EQ( v.max_size(), v.size() );
}

TEST( poor_vector, capacity )
{
	poor_vector<int,10> v;

	EXPECT_EQ( v.capacity(), 10 );

	v.push_back( 1 );

	EXPECT_EQ( v.capacity(), 10 );
}


/* Modifiers */

TEST( poor_vector, clear )
{
	poor_vector<int,10> v;

	v.clear();
	EXPECT_TRUE( v.empty() );
	EXPECT_EQ( v.size(), 0 );

	v.push_back( 1 );
	EXPECT_FALSE( v.empty() );
	EXPECT_EQ( v.size(), 1 );

	v.clear();
	EXPECT_TRUE( v.empty() );
	EXPECT_EQ( v.size(), 0 );

	v.push_back( 1 );
	v.push_back( 2 );
	v.push_back( 3 );
	v.push_back( 4 );
	EXPECT_FALSE( v.empty() );
	EXPECT_EQ( v.size(), 4 );

	v.clear();
	EXPECT_TRUE( v.empty() );
	EXPECT_EQ( v.size(), 0 );
}

TEST( poor_vector, insert )
{
	poor_vector<int,10> v { 1, 2, 3, 5, 6, 7, 8, 9, 10 };

	auto it1 = v.insert( v.begin() + 3, 4 );

	EXPECT_EQ( v.size(), 10 );
	EXPECT_EQ( content( v ), "1,2,3,4,5,6,7,8,9,10" );
	EXPECT_EQ( *it1, 4 );

	EXPECT_THROW( v.insert( v.begin(), 0 ), std::length_error );

	auto it2 = v.insert( v.begin(), 0, 0 );

	EXPECT_EQ( v.size(), 10 );
	EXPECT_EQ( content( v ), "1,2,3,4,5,6,7,8,9,10" );
	EXPECT_EQ( *it2, 1 );

	v.clear();
	auto it3 = v.insert( v.begin(), 0, 0 );

	EXPECT_EQ( it3, v.end() );

	auto it4 = v.insert( v.begin(), 4, 1 );

	EXPECT_EQ( v.size(), 4 );
	EXPECT_EQ( content( v ), "1,1,1,1" );
	EXPECT_EQ( *it4, 1 );

	auto it5 = v.insert( v.begin() + 2, 4, 2 );

	EXPECT_EQ( v.size(), 8 );
	EXPECT_EQ( content( v ), "1,1,2,2,2,2,1,1" );
	EXPECT_EQ( *it5, 2 );

	std::vector<int> src { 3, 4 };
	auto it6 = v.insert( v.begin() + 4, src.begin(), src.end() );

	EXPECT_EQ( v.size(), 10 );
	EXPECT_EQ( content( v ), "1,1,2,2,3,4,2,2,1,1" );
	EXPECT_EQ( *it6, 3 );

	v.erase( v.begin() + 6, v.end() );
	auto it7 = v.insert( v.begin() + 5, { 5, 6 } );

	EXPECT_EQ( v.size(), 8 );
	EXPECT_EQ( content( v ), "1,1,2,2,3,5,6,4" );
	EXPECT_EQ( *it7, 5 );
}

TEST( poor_vector, emplace )
{
	poor_vector<A,10> v( 2 );
	v[0] = A( "Huba!", 1 );
	v[1] = A( "Hopp!", 2 );

	auto it1 = v.emplace( v.begin() + 1, "Test", 3 );

	EXPECT_EQ( v.size(), 3 );
	EXPECT_EQ( it1->s, "Test" );
	EXPECT_EQ( it1->i, 3 );
	EXPECT_EQ( content( v ), "Huba!;1,Test;3,Hopp!;2" );
}

TEST( poor_vector, erase )
{
	poor_vector<int,10> v { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	auto it1 = v.erase( v.begin() + 3 );  // erase 4

	EXPECT_EQ( v.size(), 9 );
	EXPECT_EQ( content( v ), "1,2,3,5,6,7,8,9,10" );
	EXPECT_EQ( *it1, 5 );

	auto it2 = v.erase( v.begin() + 4, v.end() - 1 );  // erase 6..9

	EXPECT_EQ( v.size(), 5 );
	EXPECT_EQ( content( v ), "1,2,3,5,10" );
	EXPECT_EQ( *it2, 10 );

	auto it3 = v.erase( v.begin(), v.end() );

	EXPECT_EQ( v.size(), 0 );
	EXPECT_EQ( content( v ), "" );
	EXPECT_EQ( it3, v.end() );
}

TEST( poor_vector, push_back )
{
	poor_vector<std::string,2> v;

	std::string s = "Huba!";
	v.push_back( s );

	EXPECT_EQ( s, "Huba!" );
	EXPECT_EQ( v.front(), "Huba!" );
	EXPECT_EQ( v.back(), "Huba!" );

	s = "Hopp!";
	v.push_back( std::move( s ) );

	EXPECT_EQ( s, "" );
	EXPECT_EQ( v.front(), "Huba!" );
	EXPECT_EQ( v.back(), "Hopp!" );

	EXPECT_THROW( v.push_back( s ), std::length_error );
}

TEST( poor_vector, emplace_back )
{
	poor_vector<A,10> v;
	auto s = v.emplace_back( "Huba!", 1 ).s;
	auto i = v.emplace_back( "Hopp!", 2 ).i;

	EXPECT_EQ( v.size(), 2 );
	EXPECT_EQ( content( v ), "Huba!;1,Hopp!;2" );

	EXPECT_EQ( s, "Huba!" );
	EXPECT_EQ( i, 2 );
}

TEST( poor_vector, pop_back )
{
	poor_vector<std::string,2> v { "Huba!", "Hopp!" };

	EXPECT_EQ( v.front(), "Huba!" );
	EXPECT_EQ( v.back(), "Hopp!" );

	v.pop_back();

	EXPECT_EQ( v.front(), "Huba!" );
	EXPECT_EQ( v.back(), "Huba!" );

	v.pop_back();

	EXPECT_TRUE( v.empty() );
}

TEST( poor_vector, resize )
{
	poor_vector<int,10> v( 1 );

	EXPECT_EQ( v.size(), 1 );
	EXPECT_FALSE( v.empty() );

	v.resize( 5 );

	EXPECT_EQ( v.size(), 5 );
	EXPECT_FALSE( v.empty() );

	v.resize( 0 );

	EXPECT_EQ( v.size(), 0 );
	EXPECT_TRUE( v.empty() );

	v.resize( 10 );

	EXPECT_EQ( v.size(), 10 );
	EXPECT_FALSE( v.empty() );

	EXPECT_THROW( v.resize( 11 ), std::length_error );
}

TEST( poor_vector, swap )
{
	poor_vector<int,10> v1;
	v1.push_back( 1 );
	v1.push_back( 2 );
	v1.push_back( 3 );
	v1.push_back( 4 );

	poor_vector<int,10> v2;
	v2.push_back( 11 );
	v2.push_back( 12 );

	EXPECT_EQ( content( v1 ), "1,2,3,4" );
	EXPECT_EQ( content( v2 ), "11,12" );

	v1.swap( v2 );

	EXPECT_EQ( content( v2 ), "1,2,3,4" );
	EXPECT_EQ( content( v1 ), "11,12" );
}

/* Non-member functions */

TEST( poor_vector, equal )
{
	poor_vector<int,3> v1 { 1, 2, 3 };
	poor_vector<int,10> v2 { 1, 2, 3 };
	poor_vector<int,10> v3 { 1, 2, 4 };
	poor_vector<int,10> v4 { 1, 2, 3, 4 };

	EXPECT_TRUE( v1 == v2 );
	EXPECT_FALSE( v1 == v3 );
	EXPECT_FALSE( v1 == v4 );

	EXPECT_TRUE( v2 == v1 );
	EXPECT_FALSE( v2 == v3 );
	EXPECT_FALSE( v2 == v4 );

	EXPECT_FALSE( v3 == v1 );
	EXPECT_FALSE( v3 == v2 );
	EXPECT_FALSE( v3 == v4 );

	EXPECT_FALSE( v4 == v1 );
	EXPECT_FALSE( v4 == v2 );
	EXPECT_FALSE( v4 == v3 );
}

/* Standard algorithms */

TEST( poor_vector, copy_algorithm )
{
	poor_vector<int,10> v = { 1, 2, 3, 4 };

	EXPECT_EQ( content( v ), "1,2,3,4" );

	std::vector<int> dest( 3 );
	std::copy( v.begin(), v.end() - 1, dest.begin() );

	EXPECT_EQ( content( dest ), "1,2,3" );
}

TEST( poor_vector, move_algorithm )
{
	poor_vector<int,10> v = { 1, 2, 3, 4 };

	EXPECT_EQ( content( v ), "1,2,3,4" );

	auto first = v.begin(); ++first;
	auto last = v.end();
	auto d_first = v.begin();
	std::move( first, last, d_first );

	EXPECT_EQ( content( v ), "2,3,4,4" );
}

TEST( poor_vector, move_backward_algorithm )
{
	poor_vector<int,10> v = { 1, 2, 3, 4 };

	EXPECT_EQ( content( v ), "1,2,3,4" );

	auto first = v.begin();
	auto last = v.end(); --last;
	auto d_last = v.end();
	std::move_backward( first, last, d_last );

	EXPECT_EQ( content( v ), "1,1,2,3" );
}

TEST( poor_vector, sort_algorithm )
{
	poor_vector<int,10> v = { 4, 3, 1, 2 };

	EXPECT_EQ( content( v ), "4,3,1,2" );

	std::sort( v.begin(), v.end() );

	EXPECT_EQ( content( v ), "1,2,3,4" );
}

