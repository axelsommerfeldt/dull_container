/**
 * ring_queue< class T, class Container = std::vector<T> >
 * This implements a ring queue with max. capacity, dynamic growth, and random access.
 *
 * Version 2020-08-18
 * https://gitlab.com/axelsommerfeldt/dull_container
 *
 * What is the difference to std::queue?
 * - It keeps only the last <max_size> elements automatically.
 *   (If the queue is full, push() will overwrite the former front element.)
 * - It offers random access.
 * - The memory could be pre-allocated during construction or using set_max_size() later on.
 * - Once the memory for <max_size> elements is allocated, no memory functions are used during regular operation.
 * - It could be used with std::array and std::vector as data container class (but with std::deque as well).
 * - It could not be used with std::list as data container class since it uses random access internally.
 *
 * Copyright (C) 2020 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#pragma once

#include <vector>
#include <array>
#include <stdexcept>

namespace dull
{

template<
	class T,
	class Container
> class ring_queue_base
{
public:
	/* Types */

	typedef T value_type;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
	typedef value_type *pointer;
	typedef const value_type *const_pointer;
	typedef value_type &reference;
	typedef const value_type &const_reference;

	/* Constructor */

	explicit ring_queue_base( size_type max_size )
	{
		x.max_size = max_size;
		clear();
	}

	ring_queue_base( const ring_queue_base &other ) = default;

	ring_queue_base( ring_queue_base &&other ) : data( std::move( other.data ) )
	{
		x = other.x;
		other.clear();
	}

	/* Assignment */

	ring_queue_base& operator=( const ring_queue_base &other ) = default;

	ring_queue_base& operator=( ring_queue_base &&other )
	{
		data = std::move( other.data );
		x = other.x;
		other.clear();
		return *this;
	}

	/* Element access */

	reference at( size_type pos ) { range_check( pos ); return (*this)[pos]; }
	const_reference at( size_type pos ) const { range_check( pos ); return (*this)[pos]; }

	reference operator[]( size_type pos ) { return data[inc( x.begin, pos )]; }
	const_reference operator[]( size_type pos ) const { return data[inc( x.begin, pos )]; }

	reference front() { return data[x.begin]; }
	const_reference front() const { return data[x.begin]; }

	reference back() { return data[dec(x.end)]; }
	const_reference back() const { return data[dec(x.end)]; }

	/* Iterators */

	template< typename Pointer, typename Reference, typename RingQueuePointer > class my_iterator
	{
	public:
		using iterator_category = std::bidirectional_iterator_tag;  // TODO: random_access_iterator_tag
		using value_type = ring_queue_base::value_type;
		using difference_type = ring_queue_base::difference_type;
		using pointer = Pointer;
		using reference = Reference;
   
		my_iterator() : rq( nullptr ), i( 0 ), begin( false ) {}

		my_iterator &operator++() { i = rq->inc( i ); begin = false; return *this; }
		my_iterator  operator++(int) { my_iterator it( *this ); ++*this; return it; }
		my_iterator &operator--() { i = rq->dec( i ); begin = (i == rq->x.begin && !rq->empty()); return *this; }
		my_iterator  operator--(int) { my_iterator it( *this ); --*this; return it; }

		bool operator!=(const my_iterator &b) const { return rq != b.rq || i != b.i || begin != b.begin; }
		bool operator==(const my_iterator &b) const { return rq == b.rq && i == b.i && begin == b.begin; }

		reference operator*() { return rq->data[i]; }
		pointer operator->() { return &rq->data[i]; }

		friend difference_type operator-( const my_iterator &a, const my_iterator &b )  // needed by std::move()
		{
			return a.linear_index() - b.linear_index();
		}

	protected:
		using rq_pointer = RingQueuePointer;
		my_iterator( rq_pointer rq, size_type i, bool begin ) : rq( rq ), i( i ), begin( begin ) {}
		friend class ring_queue_base;

	private:
		size_type linear_index() const  // handles wrap-around and differentiate empty from full queue
		{
			auto _i = i;
			if ( _i <= rq->x.begin && !begin ) _i += rq->x.max_size;
			return _i;
		}

		rq_pointer rq;
		size_type i;
		bool begin;  // this flag is needed to distinguish begin() from end() if queue is full
	};
	typedef my_iterator< T*, T&, ring_queue_base* > iterator;
	typedef my_iterator< const T*, const T&, const ring_queue_base* > const_iterator;

	iterator begin() noexcept { return iterator( this, x.begin, !empty() ); }
	const_iterator begin() const noexcept { return const_iterator( this, x.begin, !empty() ); }
	const_iterator cbegin() const noexcept { return const_iterator( this, x.begin, !empty() ); }

	iterator end() noexcept { return iterator( this, x.end, false ); }
	const_iterator end() const noexcept { return const_iterator( this, x.end, false ); }
	const_iterator cend() const noexcept { return const_iterator( this, x.end, false ); }

	template< class Iterator > class my_reverse_iterator : public Iterator
	{
	public:
		using iterator_category = std::forward_iterator_tag;  // TODO
		using value_type = typename Iterator::value_type;
		using difference_type = typename Iterator::difference_type;
		using pointer = typename Iterator::pointer;
		using reference = typename Iterator::reference;

		my_reverse_iterator() : Iterator() {}
		my_reverse_iterator &operator++() { Iterator::i = Iterator::rq->dec( Iterator::i ); Iterator::begin = false; return *this; }
		my_reverse_iterator  operator++(int) { my_reverse_iterator it( *this ); ++*this; return it; }
		my_reverse_iterator &operator--() = delete;     // TODO
		my_reverse_iterator  operator--(int) = delete;  // TODO

		friend difference_type operator-( const my_reverse_iterator &a, const my_reverse_iterator &b ) = delete;  // TODO

	protected:
		using rq_pointer = typename Iterator::rq_pointer;
		my_reverse_iterator( rq_pointer rq, size_type i, bool begin ) : Iterator( rq, i, begin ) {}
		friend class ring_queue_base;
	};
	typedef my_reverse_iterator< iterator > reverse_iterator;
	typedef my_reverse_iterator< const_iterator > const_reverse_iterator;

	reverse_iterator rbegin() noexcept { return reverse_iterator( this, dec(x.end), !empty() ); }
	const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator( this, dec(x.end), !empty() ); }
	const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator( this, dec(x.end), !empty() ); }

	reverse_iterator rend() noexcept { return reverse_iterator( this, dec(x.begin), false ); }
	const_reverse_iterator rend() const noexcept { return const_reverse_iterator( this, dec(x.begin), false ); }
	const_reverse_iterator crend() const noexcept { return const_reverse_iterator( this, dec(x.begin), false ); }

	/* Capacity */

	bool empty() const noexcept { return x.size == 0; }
	bool full() const noexcept { return x.size == x.max_size; }
	size_type size() const noexcept { return x.size; }
	size_type max_size() const noexcept { return x.max_size; }
	size_type capacity() const noexcept { return data.size(); }

	void set_max_size( size_type max_size, size_type capacity = 0 )
	{
		// Note: This will clear the content
		// TODO: Keep all elements if the max_size will be increased
		// TODO: Keep the last ring_queue::max_size elements if the max_size will be decreased

		set_capacity( capacity );
		x.max_size = max_size;
		clear();
	}

	/* Modifiers */

	void clear() noexcept
	{
		x.begin = x.end = x.size = 0;
	}

	void push( const value_type& value )
	{
		if ( x.max_size == 0 ) return;
		next() = value;
	}
	void push( value_type&& value )
	{
		if ( x.max_size == 0 ) return;
		next() = std::move( value );
	}

	void pop()
	{
		x.begin = inc( x.begin );
		if ( --x.size == 0 ) x.begin = x.end = 0;  // if queue gets empty, reset indicies to avoid unnecessary growth of data container 
	}

	void swap( ring_queue_base& other )
	{
		data.swap( other.data );
		auto tmp = x;
		x = other.x;
		other.x = tmp;
	}

protected:
	virtual void set_capacity( size_type capacity ) = 0;

	// Increment resp. decrement internal position
	size_type inc( size_type i ) const { if ( ++i == x.max_size ) i = 0; return i; } // = add( i, 1 )
	size_type dec( size_type i ) const { if ( i == 0 ) i = x.max_size; return --i; } // = add( i, -1 )
	size_type inc( size_type i, size_type a ) const { return (i + a) % x.max_size; } // = add( i + a, 0 )
//	size_type add( size_type i, std::ptrdiff_t a ) const { return (i + x.max_size + a) % x.max_size; }

private:
	// range_check() used in at()
	void range_check( size_type pos ) const
	{
		if ( pos >= x.size )
			throw std::out_of_range( "ring_queue::range_check: pos >= size()" );
	}

	// next() used in push()
	reference &next()
	{
		if ( x.size == x.max_size )
			pop();
		else if ( x.end == data.size() )
			set_capacity( x.end + 1 );

		reference elem = data[x.end];
		x.end = inc( x.end );
		x.size++;

		return elem;
	}

protected:
	Container data;
	struct meta_data
	{
		size_type max_size;
		size_type begin, end, size;
	} x;
};

template<
	class T,
	class Container = std::vector<T>
> class ring_queue : public ring_queue_base<T, Container>
{
	typedef ring_queue_base<T, Container> base_type;

public:
	using size_type = typename base_type::size_type;

	/* Constructor */

	explicit ring_queue( size_type max_size = 0, size_type capacity = 0 )
		: base_type( max_size )
	{
		set_capacity( capacity );
	}

	void set_capacity( size_type capacity )
	{
		base_type::data.resize( capacity );

		// If we have reached the max. size make sure
		// the data container does not waste any memory.
		if ( capacity == base_type::x.max_size ) base_type::data.shrink_to_fit();
	}

	void shrink_to_fit()
	{
		if ( !base_type::full() )
		{
			/*
			 * Shift data to the left of the data container
			 *
			 * If the data is not contiguous this could need multiple passes
			 * since the amount of free items and therefore the ability to shift is limited.
			 */

			auto &x = base_type::x;
			auto to_left = x.begin;
			auto to_right = (x.begin <= x.end) ? x.max_size : x.size - x.end;
			if ( to_left <= to_right )
			{
				while ( x.begin != 0 )
				{
					size_type dest = ( x.begin <= x.end ) ? 0 : x.end;
					for ( size_type s = x.begin, d = dest; s != x.end; s = base_type::inc( s ), d = base_type::inc( d ) )
					{
						base_type::data[d] = std::move( base_type::data[s] );
					}
					x.begin = dest;
					x.end = base_type::inc( dest, x.size );
				}
			}
			else
			{
				do
				{
					size_type dest = ( x.begin >= x.size ) ? x.size : x.begin;
					for ( size_type s = base_type::dec( x.end ), d = base_type::dec( dest );; s = base_type::dec( s ), d = base_type::dec( d ) )
					{
						base_type::data[d] = std::move( base_type::data[s] );
						if ( s == x.begin )
						{
							x.begin = d;
							x.end = base_type::inc( d, x.size );
							break;
						}
					}
				}
				while ( x.begin != 0 );
			}
		}

		// Shrink data container size
		base_type::data.resize( base_type::x.size );
		base_type::data.shrink_to_fit();
	}
};

template<
	class T,
	std::size_t N
> class ring_queue<T, std::array<T,N>> : public ring_queue_base<T, std::array<T,N>>
{
	typedef ring_queue_base<T, std::array<T,N>> base_type;

public:
	using size_type = typename base_type::size_type;

	/* Constructor */

	explicit ring_queue( size_type max_size = N, size_type capacity = N )
		: base_type( max_size )
	{
		set_capacity( capacity );
	}

	void set_capacity( size_type capacity )
	{
		if ( capacity > N )
			throw std::out_of_range( "ring_queue::set_capacity: capacity > N" );
	}

	void shrink_to_fit() {}
};

}

