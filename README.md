# ring_queue< class T, class Container = std::vector<T> >

A C++ container class which offers a ring queue with max. capacity, dynamic growth, and random access.

## What is the difference to std::queue?

* It keeps only the last <max_size> elements automatically.
  (If the queue is full, push() will overwrite the former front element.)
* It offers random access.
* The memory could be pre-allocated during construction or using set_max_size() later on.
* Once the memory for <max_size> elements is allocated, no memory functions are used during regular operation.
* It could be used with std::array and std::vector as data container class (but with std::deque as well).
* It could not be used with std::list as data container class since it uses random access internally.

# poor_vector< class T, std::size_t N >

 A C++ container class which offers a vector with fixed capacity and random access.

## What is the difference to std::vector?

* The capacity is fixed, therefore no memory functions will be used at all.
* Minimal footprint: Just an std::array and an iterator keeping the current end.

# simple_set< class Key, class Container = std::vector<Key> >

A C++ container class with offers an unsorted set with no extra memory footprint.

## What is the difference to std::set or std::unsorted_set?

* It is unsorted, but without extra hash table.
* Minimal memory footprint: Just the underlaying container, nothing else.
* It could be used with std::vector, std::deque, std::list, or dull::poor_vector as data container class.

