/**
 * poor_vector< class T, std::size_t N >
 * This implements a vector with fixed capacity and random access.
 *
 * Version 2020-08-19
 * https://gitlab.com/axelsommerfeldt/dull_container
 *
 * What is the difference to std::vector?
 * - The capacity is fixed, therefore no memory functions will be used at all.
 * - Minimal footprint: Just an std::array and an iterator keeping the actual end.
 *
 * Copyright (C) 2020 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#pragma once

#include <array>
#include <stdexcept>

namespace dull
{

template<
	class T,
	std::size_t N
> class poor_vector
{
public:
	/* Types */

	typedef typename std::array<T,N>::value_type value_type;
	typedef typename std::array<T,N>::size_type size_type;
	typedef typename std::array<T,N>::difference_type difference_type;
	typedef typename std::array<T,N>::pointer pointer;
	typedef typename std::array<T,N>::const_pointer const_pointer;
	typedef typename std::array<T,N>::reference reference;
	typedef typename std::array<T,N>::const_reference const_reference;
	typedef typename std::array<T,N>::iterator iterator;
	typedef typename std::array<T,N>::const_iterator const_iterator;
	typedef typename std::array<T,N>::reverse_iterator reverse_iterator;
	typedef typename std::array<T,N>::const_reverse_iterator const_reverse_iterator;

	/* Constructor */

	poor_vector() noexcept : end_of_storage( storage.begin() ) {}
	explicit poor_vector( size_type count ) noexcept : end_of_storage( storage.begin() + count ) {}
	poor_vector( size_type count, const value_type &value ) { assign( count, value ); }
	template<typename InputIterator, typename = std::_RequireInputIter<InputIterator>>
		poor_vector( InputIterator first, InputIterator last ) { assign( first, last ); }

	poor_vector( const poor_vector &other ) : storage( other.storage ), end_of_storage( storage.begin() + other.size() ) {}
	poor_vector( poor_vector &&other ) : storage( std::move( other.storage ) ), end_of_storage( storage.begin() + other.size() ) { other.clear(); }

	poor_vector( std::initializer_list<T> init ) : end_of_storage( storage.begin() )
	{
		for ( auto it = init.begin(); it != init.end(); ++it ) push_back( *it );
	}

	/* Assignment */

	poor_vector &operator=( const poor_vector &other )
	{
		storage = other.storage;
		end_of_storage = storage.begin() + other.size();
		return *this;
	}
	poor_vector &operator=( poor_vector &&other )
	{
		storage = std::move( other.storage );
		end_of_storage = storage.begin() + other.size();
		other.clear();
		return *this;
	}
	poor_vector &operator=( std::initializer_list<T> ilist )
	{
		assign( ilist );
		return *this;
	}

	void assign( size_type count, const value_type &value )
	{
		capacity_check( count );
		clear();
		for ( size_type i = 0; i < count; i++ ) *end_of_storage++ = value;
	}
	template<typename InputIterator, typename = std::_RequireInputIter<InputIterator>>
	void assign( InputIterator first, InputIterator last )
	{
		clear();
		while ( first != last ) push_back( *first++ );
	}
	void assign( std::initializer_list<T> ilist )
	{
		clear();
		for ( const auto &i : ilist ) push_back( i );
	}

	/* Element access */

	reference at( size_type pos ) { range_check( pos ); return (*this)[pos]; }
	const_reference at( size_type pos ) const { range_check( pos ); return (*this)[pos]; }
	reference operator[]( size_type pos ) { return storage[pos]; }
	const_reference operator[]( size_type pos ) const { return storage[pos]; }

	reference front() { return storage.front(); }
	const_reference front() const { return storage.front(); }
	reference back() { return end_of_storage[-1]; }
	const_reference back() const { return end_of_storage[-1]; }

	pointer data() noexcept { return storage.data(); }
	const_pointer data() const noexcept { return storage.data(); }

	/* Iterators */

	iterator begin() noexcept { return storage.begin(); }
	const_iterator begin() const noexcept { return storage.begin(); }
	const_iterator cbegin() const noexcept { return storage.begin(); }

	iterator end() noexcept { return end_of_storage; }
	const_iterator end() const noexcept { return end_of_storage; }
	const_iterator cend() const noexcept { return end_of_storage; }

#if 0  // TODO
	reverse_iterator rbegin() noexcept { return reverse_iterator( this, dec(x.end), !empty() ); }
	const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator( this, dec(x.end), !empty() ); }
	const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator( this, dec(x.end), !empty() ); }

	reverse_iterator rend() noexcept { return reverse_iterator( this, dec(x.begin), false ); }
	const_reverse_iterator rend() const noexcept { return const_reverse_iterator( this, dec(x.begin), false ); }
	const_reverse_iterator crend() const noexcept { return const_reverse_iterator( this, dec(x.begin), false ); }
#endif

	/* Capacity */

	bool empty() const noexcept { return storage.begin() == end_of_storage; }
	size_type size() const noexcept { return end_of_storage - storage.begin(); }
	size_type max_size() const noexcept { return N; }
	size_type capacity() const noexcept { return N; }

	void reserve( size_type new_capacity ) { capacity_check( new_capacity ); }
	void shrink_to_fit() {}

	/* Modifiers */

	void clear() noexcept
	{
		end_of_storage = storage.begin();
	}

	iterator insert( const_iterator pos, const T& value )
	{
		auto it = shift( pos, 1 );
		*it = value;
		return it;
	}
	iterator insert( const_iterator pos, T&& value )
	{
		auto it = shift( pos, 1 );
		*it = std::move( value );
		return it;
	}
	iterator insert( const_iterator pos, size_type count, const T& value )
	{
		for ( auto it = shift( pos, count ); count != 0; count-- ) *it++ = value;
		return const_cast<iterator>( pos );
	}
	template<typename InputIterator, typename = std::_RequireInputIter<InputIterator>>
	iterator insert( const_iterator pos, InputIterator first, InputIterator last )
	{
		auto it = shift( pos, last - first );
		while ( first != last ) *it++ = *first++;
		return const_cast<iterator>( pos );
	}
	iterator insert( const_iterator pos, std::initializer_list<T> ilist )
	{
		auto it = shift( pos, ilist.size() );
		for ( const auto &i : ilist ) *it++ = i;
		return const_cast<iterator>( pos );
	}

	template<class... Args>
	iterator emplace( const_iterator pos, Args&&... args )
	{
		auto it = shift( pos, 1 );
		// Note: Since our storage is already initialized we cannot construct the element through std::allocator_traits::construct
		*it = T( std::forward<Args>(args)... );
		return it;
	}

	iterator erase( const_iterator pos )
	{
		return erase( pos, pos + 1 );
	}
	iterator erase( const_iterator first, const_iterator last )
	{
		for ( auto s = const_cast<iterator>( last ), t = const_cast<iterator>( first ); s != end_of_storage; ) *t++ = *s++;
		end_of_storage -= last - first;
		return const_cast<iterator>( first );
	}

	void push_back( const value_type &value )
	{
		capacity_check( size() + 1 );
		*end_of_storage++ = value;
	}
	void push_back( value_type &&value )
	{
		capacity_check( size() + 1 );
		*end_of_storage++ = std::move( value );
	}

	template<class... Args>
	reference emplace_back( Args&&... args )
	{
		capacity_check( size() + 1 );
		// Note: Since our storage is already initialized we cannot construct the element through std::allocator_traits::construct
		return *end_of_storage++ = T( std::forward<Args>(args)... );
	}

	void pop_back()
	{
		--end_of_storage;
	}

	void resize( size_type count )
	{
		capacity_check( count );
		// while ( size() < count ) *end_of_storage++ = T();
		end_of_storage = storage.begin() + count;
	}
	void resize( size_type count, const value_type &value )
	{
		capacity_check( count );
		while ( size() < count ) *end_of_storage++ = value;
		end_of_storage = storage.begin() + count;
	}

	void swap( poor_vector &other ) noexcept
	{
		storage.swap( other.storage );
		auto this_size = size();
		end_of_storage = storage.begin() + other.size();
		other.end_of_storage = other.begin() + this_size;
	}

private:
	// range_check() is used by at()
	void range_check( size_type pos ) const
	{
		if ( pos >= size() )
			throw std::out_of_range( "poor_vector::range_check: pos >= size()" );
	}

	// capacity_check() is used by reserve(), resize(), push_back(), and emplace_back()
	void capacity_check( size_type n ) const
	{
		if ( n > N )
			throw std::length_error( "poor_vector::capacity_check: n > N" );
	}

	// shift() is used by insert() and emplace()
	iterator shift( const_iterator pos, size_type count )
	{
		capacity_check( size() + count );

		for ( auto s = end_of_storage, t = s + count; s != pos; ) *--t = *--s;
		end_of_storage += count;

		return const_cast<iterator>( pos );
	}

protected:
	std::array<T,N> storage;
	typename std::array<T,N>::iterator end_of_storage;
};

}

/* Non-member functions */

template<class T, std::size_t N, std::size_t M>
inline bool operator==( const dull::poor_vector<T,N> &lhs, const dull::poor_vector<T,M> &rhs )
{
	return lhs.size() == rhs.size() && std::equal( lhs.begin(), lhs.end(), rhs.begin() );
}

#if __cpp_lib_three_way_comparison

template<class T, std::size_t N, std::size_t M>
inline __detail::__synth3way_t<T> operator<=>(const dull::poor_vector<T, N> &lhs, const dull::poor_vector<T, M> &rhs )
{
	return std::lexicographical_compare_three_way( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), __detail::__synth3way );
}

#else

template<class T, std::size_t N, std::size_t M>
inline bool operator!=( const dull::poor_vector<T,N> &lhs, const dull::poor_vector<T,M> &rhs )
{
	return !( lhs == rhs );
}

#endif

namespace std
{
template<class T, std::size_t N>
inline void swap( dull::poor_vector<T,N> &lhs, dull::poor_vector<T,N> &rhs ) noexcept { lhs.swap( rhs ); }
}

