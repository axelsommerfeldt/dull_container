/**
 * simple_set< class Key, class Container = std::vector<Key> >
 * This implements an unsorted set with no extra memory footprint.
 *
 * Version 2020-09-06
 * https://gitlab.com/axelsommerfeldt/dull_container
 *
 * What is the difference to std::set or std::unsorted_set?
 * - It is unsorted, but without extra hash table.
 * - Minimal memory footprint: Just the underlaying container, nothing else.
 * - It could be used with std::vector, std::deque, std::list, or dull::poor_vector as data container class.
 *
 * Copyright (C) 2020 by Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 * THIS SOFTWARE.
 */

#pragma once

#include <vector>

namespace dull
{

template<
	class Key,
	class Container = std::vector<Key>
> class simple_set
{
public:
	/* Types */

	typedef Key key_type;
	typedef typename Container::value_type value_type;
	typedef typename Container::size_type size_type;
	typedef typename Container::difference_type difference_type;
	typedef typename Container::pointer pointer;
	typedef typename Container::const_pointer const_pointer;
	typedef typename Container::reference reference;
	typedef typename Container::const_reference const_reference;
	typedef typename Container::iterator iterator;
	typedef typename Container::const_iterator const_iterator;
	typedef typename Container::reverse_iterator reverse_iterator;
	typedef typename Container::const_reverse_iterator const_reverse_iterator;

	/* Constructor */

	simple_set() noexcept {}
	template<typename InputIterator, typename = std::_RequireInputIter<InputIterator>>
		simple_set( InputIterator first, InputIterator last ) { assign( first, last ); }

	simple_set( const simple_set &other ) = default;
	simple_set( simple_set &&other ) = default;

	simple_set( std::initializer_list<Key> init )
	{
		for ( auto it = init.begin(); it != init.end(); ++it ) insert( *it );
	}

	/* Assignment */

	simple_set &operator=( const simple_set &other ) = default;
	simple_set &operator=( simple_set &&other ) = default;

	simple_set &operator=( std::initializer_list<Key> ilist )
	{
		assign( ilist );
		return *this;
	}

	template<typename InputIterator, typename = std::_RequireInputIter<InputIterator>>
	void assign( InputIterator first, InputIterator last )
	{
		clear();
		for ( ; first != last; ++first ) insert( *first );
	}
	void assign( std::initializer_list<Key> ilist )
	{
		clear();
		for ( const auto &i : ilist ) insert( i );
	}

	/* Element access */

	Container &container() { return storage; }
	const Container &container() const { return storage; }

	/* Iterators */

	iterator begin() noexcept { return storage.begin(); }
	const_iterator begin() const noexcept { return storage.begin(); }
	const_iterator cbegin() const noexcept { return storage.begin(); }

	iterator end() noexcept { return storage.end(); }
	const_iterator end() const noexcept { return storage.end(); }
	const_iterator cend() const noexcept { return storage.cend(); }

	reverse_iterator rbegin() noexcept { return storage.rbegin(); }
	const_reverse_iterator rbegin() const noexcept { return storage.rbegin(); }
	const_reverse_iterator crbegin() const noexcept { return storage.rbegin(); }

	reverse_iterator rend() noexcept { return storage.end(); }
	const_reverse_iterator rend() const noexcept { return storage.end(); }
	const_reverse_iterator crend() const noexcept { return storage.end(); }

	/* Capacity */

	bool empty() const noexcept { return storage.empty(); }
	size_type size() const noexcept { return storage.size(); }
	size_type max_size() const noexcept { return storage.max_size(); }

	/* Modifiers */

	void clear() noexcept { return storage.clear(); }

	void insert( const value_type& value )
	{
		if ( !contains( value ) )
			storage.push_back( value );
	}
	void insert( value_type&& value )
	{
		if ( !contains( value ) )
			storage.push_back( std::move(value) );
	}
	iterator insert( const_iterator hint, const value_type& value )
	{
		iterator it = find( value );
		if ( it == end() ) it = storage.insert( hint, value );
		return it;
	}
	iterator insert( const_iterator hint, value_type&& value )
	{
		iterator it = find( value );
		if ( it == end() ) it = storage.insert( hint, std::move(value) );
		return it;
	}
	template<typename InputIterator, typename = std::_RequireInputIter<InputIterator>>
	void insert( InputIterator first, InputIterator last )
	{
		for ( ; first != last; ++first ) insert( *first );
	}
	iterator insert( const_iterator pos, std::initializer_list<Key> ilist )
	{
		for ( const auto &i : ilist ) insert( i );
	}

	iterator erase( const_iterator pos ) { return storage.erase( pos ); }
	iterator erase( iterator pos ) { return storage.erase( pos ); }
	iterator erase( const_iterator first, const_iterator last ) { return storage.erase( first, last ); }
	size_type erase( const key_type& key )
	{
		auto it = find( key );
		if ( it == storage.end() ) return 0;
		erase( it );
		return 1;
	}

	void swap( simple_set &other ) noexcept { storage.swap( other.storage ); }

	/* Lookup */

	size_type count( const key_type& key ) const { return contains( key ) ? 1 : 0; }

	iterator find( const key_type& key )
	{
		iterator it = begin();
		while ( it != end() && !(*it == key) ) ++it;
		return it;
	}
	const_iterator find( const key_type& key ) const
	{
		const_iterator it = begin();
		while ( it != end() && !(*it == key) ) ++it;
		return it;
	}

	bool contains( const key_type& key ) const
	{
		return find( key ) != end();
	}

protected:
	Container storage;
};

}

/* Non-member functions */

template< class Key, class Container >
inline bool operator==( const dull::simple_set<Key,Container> &lhs, const dull::simple_set<Key,Container> &rhs )
{
	auto lhs_size = lhs.size();
	if ( lhs_size != rhs.size() ) return false;

	if ( lhs_size > 0 )
	{
		// Limit search scope on other set
		// (This only helps if both sets are nearly equal sorted.)
		auto b = rhs.begin();
		auto e = rhs.end(); --e;

		for ( const auto &key : lhs )
		{
			for ( auto it = b;; ++it )
			{
				if ( *it == key )
				{
					if ( it == b ) ++b;
					else if ( it == e ) --e;
					break;
				}

				if ( it == e ) return false;
			}
		}
	}

	return true;
}

template< class Key, class Container >
inline bool operator!=( const dull::simple_set<Key,Container> &lhs, const dull::simple_set<Key,Container> &rhs )
{
	return !( lhs == rhs );
}

namespace std
{
template< class Key, class Container >
inline void swap( dull::simple_set<Key,Container> &lhs, dull::simple_set<Key,Container> &rhs ) noexcept { lhs.swap( rhs ); }
}

